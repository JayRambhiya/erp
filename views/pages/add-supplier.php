<?php
require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | Add Supplier";
$sidebarSection = "supplier";
$sidebarSubSection = "add";
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}
$old = "";
if(Session::hasSession('old'))
{
  $old = Session::getSession('old');
  Session::unsetSession('old');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require_once __DIR__ . "/../includes/head-section.php";
  ?>

  <!--PLACE TO ADD YOUR CUSTOM CSS-->

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Customer</h1>
            <a href="<?= BASEPAGES; ?>manage-supplier.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fa fa-list-ul fa-sm text-white-75"></i> Manage Supplier
            </a>
          </div>

          <div class="row">

            <div class="col-lg-12">

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Add Supplier</h6>
                </div>
                <div class="card-body">
                  <div class="col-md-12">

                    <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="add-supplier">
                        <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                        <div class="form-row">
                            <!--FORM GROUP FIRSTNAME-->
                            <div class="form-group col-md-6">
                                <label for="first_name">First Name</label>
                                <input  type="text" 
                                        name="first_name" 
                                        id="first_name" 
                                        class="form-control <?= $errors!='' && $errors->has('first_name') ? 'error' : '';?>"
                                        placeholder = "Enter First Name"
                                        value="<?=$old != '' && isset($old['first_name']) ?$old['first_name']: '';?>"/>
                                <?php
                                if($errors!="" && $errors->has('first_name'))
                                {
                                    echo "<span class='error'>{$errors->first('first_name')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP FIRSTNAME-->

                            <!--FORM GROUP LASTNAME-->
                            <div class="form-group col-md-6">
                                <label for="last_name">Last Name</label>
                                <input  type="text" 
                                        name="last_name" 
                                        id="last_name" 
                                        class="form-control <?= $errors!='' && $errors->has('last_name') ? 'error' : '';?>"
                                        placeholder = "Enter Last Name"
                                        value="<?=$old != '' && isset($old['last_name']) ?$old['last_name']: '';?>"/>
                                <?php
                                if($errors!="" && $errors->has('last_name'))
                                {
                                    echo "<span class='error'>{$errors->first('last_name')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP LASTNAME-->
                        </div>

                        <div class="form-row">
                            <!--FORM GROUP PHONENO-->
                            <div class="form-group col-md-6">
                                <label for="phone_no">Phone Number</label>
                                <input  type="text" 
                                        name="phone_no" 
                                        id="phone_no" 
                                        class="form-control <?= $errors!='' && $errors->has('phone_no') ? 'error' : '';?>"
                                        placeholder = "Enter Phone Number"
                                        value="<?=$old != '' && isset($old['phone_no']) ?$old['phone_no']: '';?>"/>
                                <?php
                                if($errors!="" && $errors->has('phone_no'))
                                {
                                    echo "<span class='error'>{$errors->first('phone_no')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP PHONENO-->

                            <!--FORM GROUP EMAIL-->
                            <div class="form-group col-md-6">
                                <label for="email_id">Email</label>
                                <input  type="text" 
                                        name="email_id" 
                                        id="email_id" 
                                        class="form-control <?= $errors!='' && $errors->has('email_id') ? 'error' : '';?>"
                                        placeholder = "Enter Email"
                                        value="<?=$old != '' && isset($old['email_id']) ?$old['email_id']: '';?>"/>
                                <?php
                                if($errors!="" && $errors->has('email_id'))
                                {
                                    echo "<span class='error'>{$errors->first('email_id')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP EMAIL-->
                        </div>

                        <div class="form-row">
                            <!--FORM GROUP GSTNO-->
                            <div class="form-group col-md-6">
                                <label for="gst_no">GST Number</label>
                                <input  type="text" 
                                        name="gst_no" 
                                        id="gst_no" 
                                        class="form-control <?= $errors!='' && $errors->has('gst_no') ? 'error' : '';?>"
                                        placeholder = "Enter GST Number"
                                        value="<?=$old != '' && isset($old['gst_no']) ?$old['gst_no']: '';?>"/>
                                <?php
                                if($errors!="" && $errors->has('gst_no'))
                                {
                                    echo "<span class='error'>{$errors->first('gst_no')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP GSTNO-->
                        </div>
                        <div class="form-row">
                          <!--FORM GROUP BLOCKID-->
                          <div class="form-group col-md-6">
                              <label for="block_id">Block</label>
                              <input type="text" 
                              placeholder = "Enter suppliers Block number"
                              name="block_id"
                              id="block_id"
                              class = "form-control" 
                              value = "<?=$old != '' && isset($old['block_id']) ?$old['block_id']: '';?>">
                              <?php
                              if($errors!="" && $errors->has('block_id'))
                              {
                                  echo "<span class='error'>{$errors->first('block_id')}</span>";
                              }
                              ?>
                          </div>
                          <!--/FORM GROUP BLOCKID-->
                          
                          <!--FORM GROUP STREET-->
                          <div class="form-group col-md-6">
                              <label for="street">Street</label>
                              <input type="text" 
                              placeholder = "Enter supplier Street Name"
                              name="street"
                              id="street"
                              class = "form-control" 
                              value = "<?=$old != '' && isset($old['street']) ?$old['street']: '';?>">
                              <?php
                              if($errors!="" && $errors->has('street'))
                              {
                                  echo "<span class='error'>{$errors->first('street')}</span>";
                              }
                              ?>
                          </div>
                          <!--/FORM GROUP STREET-->
                        </div>

                        <div class="form-row">
                          <!--FORM GROUP CITY-->
                          <div class="form-group col-md-6">
                              <label for="city">City</label>
                              <input type="text" 
                              placeholder = "Enter supplier City"
                              name="city"
                              id="city"
                              class = "form-control" 
                              value = "<?=$old != '' && isset($old['city']) ?$old['city']: '';?>">
                              <?php
                              if($errors!="" && $errors->has('city'))
                              {
                                  echo "<span class='error'>{$errors->first('city')}</span>";
                              }
                              ?>
                          </div>
                          <!--/FORM GROUP CITY-->
                          
                          <!--FORM GROUP PINCODE-->
                          <div class="form-group col-md-6">
                              <label for="pincode">Pincode</label>
                              <input type="text" 
                              placeholder = "Enter supplier Pincode"
                              name="pincode"
                              id="pincode"
                              class = "form-control" 
                              value = "<?=$old != '' && isset($old['pincode']) ?$old['pincode']: '';?>">
                              <?php
                              if($errors!="" && $errors->has('pincode'))
                              {
                                  echo "<span class='error'>{$errors->first('pincode')}</span>";
                              }
                              ?>
                          </div>
                          <!--/FORM GROUP PINCODE-->
                        </div>

                        <div class="form-row">
                          <!--FORM GROUP STATE-->
                          <div class="form-group col-md-6">
                              <label for="state">State</label>
                              <input type="text" 
                              placeholder = "Enter supplier State"
                              name="state"
                              id="state"
                              class = "form-control" 
                              value = "<?=$old != '' && isset($old['state']) ?$old['state']: '';?>">
                              <?php
                              if($errors!="" && $errors->has('state'))
                              {
                                  echo "<span class='error'>{$errors->first('state')}</span>";
                              }
                              ?>
                          </div>
                          <!--/FORM GROUP STATE-->
                          
                          <!--FORM GROUP COUNTRY-->
                          <div class="form-group col-md-6">
                              <label for="county">Country</label>
                              <input type="text" 
                              placeholder = "Enter supplier Country"
                              name="county"
                              id="county"
                              class = "form-control" 
                              value = "<?=$old != '' && isset($old['county']) ?$old['county']: '';?>">
                              <?php
                              if($errors!="" && $errors->has('county'))
                              {
                                  echo "<span class='error'>{$errors->first('county')}</span>";
                              }
                              ?>
                          </div>
                          <!--/FORM GROUP COUNTRY-->
                        </div>
                      <button type="submit" class="btn btn-primary" name="add_supplier" value="addSupplier"><i class="fa fa-check"></i> Submit</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>
  
  <!--PAGE LEVEL SCRIPTS-->
  <?php require_once(__DIR__ . "/../includes/page-level/supplier/add-supplier-scripts.php");?>
</body>

</html>