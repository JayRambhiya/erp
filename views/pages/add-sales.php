<?php
require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | Add New Sales";
$sidebarSection = "transaction";
$sidebarSubSection = "sales";
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
// unserialize() is method to unserialize the serialized object. To unset a serialized oject from the SESSION array it needs to unserialized.
  Session::unsetSession('errors');
}
$old = "";
if(Session::hasSession('old'))
{
  $old = Session::getSession('old');
  Session::unsetSession('old');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require_once __DIR__ . "/../includes/head-section.php";
  ?>

  <!--PLACE TO ADD YOUR CUSTOM CSS-->
  <style>
    .email-verify{
        background:green;
        color:#FFF;
        padding: 5px,10px;
        font-size: .875rem;
        line-height: 1.5;
        border-radius: .2rem;
        vertical-align: middle;
        /* display:none!important; */
    }
  </style>
</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Sales</h1>
          </div>

        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card shadow mb-4">
                <!--Card Header-->
                <div class="card-header py-3 d-flex flex-row justify-content-end">
                    <div class="mr-3">
                        <input type="text" class = "form-control" name = "email" id = "customer_email" placeholder = "Enter Email of Customer">
                    </div>
                    <div>
                        <p class = "email-verify d-none" id = "email_verify_success">
                            <i class = "fas fa-check fa-sm text-white mr-1"></i>Email Verified
                        </p>
                        <p class = "email-verify bg-danger d-none mb-0" id = "email_verify_fail">
                            <i class = "fas fa-times fa-sm text-white mr-1"></i>Email Not Verified
                        </p>
                        <a href="<?= BASEPAGES;?>add-customer.php" class = "btn btn-sm btn-warning shadow-sm d-none" id = "add_customer_btn">
                            <i class = "fas fa-users fa-sm text-white"></i>Add Customer
                        </a>
                        <button type = "button" class = "d-sm-inline-block btn btn-primary shadow-sm" name = "check_email" id = "check_email" onclick = "verifyEmail()">
                            <i class = "fas fa-envelope fa-sm text-white"></i> Check Email
                        </button>
                    </div>
                </div>
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class = "m-0 font-weight-bold text-primary">
                        <i class = "fas fa-plus"></i>Sales
                    </h6>
                    <button type = "button" onclick = "addProduct();" class = "btn btn-primary d-sm-inline">
                    Add Product
                    </button>
                </div>

                <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="add-sales">
                    <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                    <input type = "hidden" name="customer_id" id="customer_id" type="text">
                    <!-- CARD BODY -->
                    <div class="card-body">
                        <div id = "products_container">
                            <!--BEGIN: PRODUCT CUSTOM CONTROL-->
                            <div class="row product_row" id = "element_1">
                                <!-- BEGIN: CATEGORY SELECT -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Category </label>
                                        <select id="category_1" class = "form-control category_select">
                                            <option disabled selected>Select Category</option>
                                            <?php
                                                $categories = $di->get('database')->readData("category",['id','name'],"deleted=0");
                                                foreach($categories as $category){
                                                    echo "<option value = '{$category->id}'>{$category->name}</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <!-- END: CATEGORY SELECT -->

                                <!-- BEGIN: PRODUCT SELECT -->
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="">Products </label>
                                        <select name = "product_id[]" id="product_1" class = "form-control product_select">
                                            <option disabled selected>Select Product</option>  
                                        </select>
                                    </div>
                                </div>
                                <!-- END: PRODUCT SELECT -->

                                <!-- BEGIN: SELLING PRICE -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="">Selling Price</label>
                                        <input type="text" id = "selling_price_1" class = "form-control selling_price_input" disabled value = "0">
                                    </div>
                                </div>
                                <!-- END: SELLING PRICE -->

                                <!-- BEGIN: QUANTITY -->
                                <div class="col-md-1">
                                    <div class="form-group">
                                    <label for="">Quantity</label>
                                        <input type="number" name = "quantity[]" id = "quantity_1" class = "form-control quantity_input" min = "0"value = 0>
                                    </div>
                                </div>
                                <!-- END: QUANTITY -->

                                <!-- BEGIN: DISCOUNT -->
                                <div class="col-md-1">
                                    <div class="form-group">
                                    <label for="">Discount(%)</label>
                                        <input type="number" name = "discount[]" id = "discount_1" class = "form-control discount_input" min = "0"value = 0>
                                    </div>
                                </div>
                                <!-- END: DISCOUNT -->

                                <!-- BEGIN: FINAL RATE -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="">Final Rate</label>
                                        <input type="number" id = "finalrate_1" class = "form-control finalrate_input" value = 0 readonly>
                                    </div>
                                </div>
                                <!-- END: DISCOUNT -->

                                <!--BEGIN: DELETE BUTTON -->
                                <div class="col-md-1">
                                    <button onclick = "deleteProduct(1)" type = "button" class = "btn btn-danger" style = "margin-top: 40%" id = "deletebutton_1">
                                    <i class = "far fa-trash-alt"></i>
                                    </button>
                                </div>
                                <!--END: DELETE BUTTON -->
                            </div>
                            <!--END: PRODUCT CUSTOM CONTROL-->
                        </div>
                    </div>
                    <!-- END: CARD BODY -->
                    <!--BEGIN: CARD FOOTER-->
                    <div class="card-footer d-flex justify-content-between">
                        <div>
                            <input type="submit" class="btn btn-primary" id = "add_sales" name="add_sales" value="Submit" disabled>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Final Total</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="finalTotal" value="0">
                            </div>
                        </div>
                    </div>
                    <!--END: CARD FOOTER-->
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End of Main Content -->
      <!-- Footer -->
      <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>
  
  <!--PAGE LEVEL SCRIPTS-->
  <?php require_once(__DIR__ . "/../includes/page-level/transactions/add-sale-scripts.php");?>
</body>

</html>