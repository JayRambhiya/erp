<?php
require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | Add Customer";
// $sidebarSection = "customer";
// $sidebarSubSection = "manage";
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}
$old = "";
if(Session::hasSession('old'))
{
  $old = Session::getSession('old');
  Session::unsetSession('old');
}
if(isset($_GET['edit_id'])){
    $customer_id = $_GET['edit_id'];
    $query = "SELECT `customers`.`id`, `customers`.`first_name`, `customers`.`last_name`, `customers`.`gst_no`, `customers`.`phone_no`, `customers`.`email_id`, `customers`.`gender`,`address_customer`.`address_id` ,`address`.`block_no`,`address`.`street`,`address`.`city`,`address`.`pincode`,`address`.`state`,`address`.`country`,`address`.`town`FROM `customers` JOIN `address_customer` ON `address_customer`.`customer_id` = `customers`.`id` JOIN `address` ON `address`.`id` = `address_customer`.`address_id` WHERE `customers`.`deleted` = 0 AND `customers`.`id` = {$customer_id}";
    $result = $di->get('database')->raw($query);
    // Util::dd($result[0]);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php  require_once __DIR__ . "/../includes/head-section.php"; ?>
  <!--PLACE TO ADD YOUR CUSTOM CSS-->
  <link rel="stylesheet" href="<?=BASEASSETS;?>vendor/toastr/toastr.min.css">
  <!-- <link href="<?= BASEASSETS; ?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet"> -->
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php require_once(__DIR__. "/../includes/sidebar.php");?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php //require_once(__DIR__. "/../includes/navbar.php");?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Customer</h1>
            <a href="<?= BASEPAGES;?>manage-customer.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fa fa-plus fa-sm text-white-75"></i> Delete Customer
            </a>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">View Customer</h6>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="edit-manage-customer">
                        <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                        <input type="hidden" name="customer_id" value="<?= $customer_id?>">
                        <div class="form-row">
                            <!--FORM GROUP FIRSTNAME-->
                            <div class="form-group col-md-6">
                                <label for="first_name">First Name</label>
                                <input  type="text" 
                                        name="first_name" 
                                        id="first_name" 
                                        class="form-control <?= $errors!='' && $errors->has('first_name') ? 'error' : '';?>"
                                        placeholder = "Enter First Name"
                                        value="<?=$result[0]->first_name?>"/>
                                <?php
                                if($errors!="" && $errors->has('first_name'))
                                {
                                    echo "<span class='error'>{$errors->first('first_name')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP FIRSTNAME-->

                            <!--FORM GROUP LASTNAME-->
                            <div class="form-group col-md-6">
                                <label for="last_name">Last Name</label>
                                <input  type="text" 
                                        name="last_name" 
                                        id="last_name" 
                                        class="form-control <?= $errors!='' && $errors->has('last_name') ? 'error' : '';?>"
                                        placeholder = "Enter Last Name"
                                        value="<?=$result[0]->last_name?>"/>
                                <?php
                                if($errors!="" && $errors->has('last_name'))
                                {
                                    echo "<span class='error'>{$errors->first('last_name')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP LASTNAME-->
                        </div>

                        <div class="form-row">
                            <!--FORM GROUP PHONENO-->
                            <div class="form-group col-md-6">
                                <label for="phone_no">Phone Number</label>
                                <input  type="text" 
                                        name="phone_no" 
                                        id="phone_no" 
                                        class="form-control <?= $errors!='' && $errors->has('phone_no') ? 'error' : '';?>"
                                        placeholder = "Enter Phone Number"
                                        value="<?=$result[0]->phone_no?>"/>
                                <?php
                                if($errors!="" && $errors->has('phone_no'))
                                {
                                    echo "<span class='error'>{$errors->first('phone_no')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP PHONENO-->

                            <!--FORM GROUP EMAIL-->
                            <div class="form-group col-md-6">
                                <label for="email_id">Email</label>
                                <input  type="text" 
                                        name="email_id" 
                                        id="email_id" 
                                        class="form-control <?= $errors!='' && $errors->has('email_id') ? 'error' : '';?>"
                                        placeholder = "Enter Email"
                                        value="<?=$result[0]->email_id?>"/>
                                <?php
                                if($errors!="" && $errors->has('email_id'))
                                {
                                    echo "<span class='error'>{$errors->first('email_id')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP EMAIL-->
                        </div>

                        <div class="form-row">
                            <!--FORM GROUP GSTNO-->
                            <div class="form-group col-md-6">
                                <label for="gst_no">GST Number</label>
                                <input  type="text" 
                                        name="gst_no" 
                                        id="gst_no" 
                                        class="form-control <?= $errors!='' && $errors->has('gst_no') ? 'error' : '';?>"
                                        placeholder = "Enter GST Number"
                                        value="<?=$result[0]->gst_no?>"/>
                                <?php
                                if($errors!="" && $errors->has('gst_no'))
                                {
                                    echo "<span class='error'>{$errors->first('gst_no')}</span>";
                                }
                                ?>
                            </div>
                            <!--/FORM GROUP GSTNO-->

                            <!--FORM GROUP GENDER-->
                            <div class="form-group col-md-6">
                                <label for="gender">Gender</label>
                                <select name="gender" id="gender" class = "form-control">
                                    <option <?=$result[0]->gender == 'Male' ? ' selected="selected"' : '';?> value="male">Male</option>
                                    <option <?=$result[0]->gender == 'Female' ? ' selected="selected"' : '';?> value="female">Female</option>
                                </select>                                        
                                <?php
                                if($errors!="" && $errors->has('gender'))
                                {
                                    echo "<span class='error'>{$errors->first('gender')}</span>";
                                }
                                ?>
                        </div>
                            <!--/FORM GROUP GENDER-->
                        </div>
                        <div class="form-row">
                        <!--FORM GROUP BLOCKNO-->
                        <div class="form-group col-md-6">
                            <label for="block_no">Block</label>
                            <input type="text" 
                            placeholder = "Enter customers Block number"
                            name="block_no"
                            id="block_no"
                            class = "form-control" 
                            value = "<?=$result[0]->block_no?>">
                            <?php
                            if($errors!="" && $errors->has('block_no'))
                            {
                                echo "<span class='error'>{$errors->first('block_no')}</span>";
                            }
                            ?>
                        </div>
                        <!--/FORM GROUP BLOCKNO-->
                        
                        <!--FORM GROUP STREET-->
                        <div class="form-group col-md-6">
                            <label for="street">Street</label>
                            <input type="text" 
                            placeholder = "Enter customers Street Name"
                            name="street"
                            id="street"
                            class = "form-control" 
                            value = "<?=$result[0]->street?>">
                            <?php
                            if($errors!="" && $errors->has('street'))
                            {
                                echo "<span class='error'>{$errors->first('street')}</span>";
                            }
                            ?>
                        </div>
                        <!--/FORM GROUP STREET-->
                        </div>

                        <div class="form-row">
                        <!--FORM GROUP CITY-->
                        <div class="form-group col-md-6">
                            <label for="city">City</label>
                            <input type="text" 
                            placeholder = "Enter customers City"
                            name="city"
                            id="city"
                            class = "form-control" 
                            value = "<?=$result[0]->city?>">
                            <?php
                            if($errors!="" && $errors->has('city'))
                            {
                                echo "<span class='error'>{$errors->first('city')}</span>";
                            }
                            ?>
                        </div>
                        <!--/FORM GROUP CITY-->
                        
                        <!--FORM GROUP PINCODE-->
                        <div class="form-group col-md-6">
                            <label for="pincode">Pincode</label>
                            <input type="text" 
                            placeholder = "Enter customers Pincode"
                            name="pincode"
                            id="pincode"
                            class = "form-control" 
                            value = "<?=$result[0]->pincode?>">
                            <?php
                            if($errors!="" && $errors->has('pincode'))
                            {
                                echo "<span class='error'>{$errors->first('pincode')}</span>";
                            }
                            ?>
                        </div>
                        <!--/FORM GROUP PINCODE-->
                        </div>

                        <div class="form-row">
                        <!--FORM GROUP STATE-->
                        <div class="form-group col-md-6">
                            <label for="state">State</label>
                            <input type="text" 
                            placeholder = "Enter customers State"
                            name="state"
                            id="state"
                            class = "form-control" 
                            value = "<?=$result[0]->state?>">
                            <?php
                            if($errors!="" && $errors->has('state'))
                            {
                                echo "<span class='error'>{$errors->first('state')}</span>";
                            }
                            ?>
                        </div>
                        <!--/FORM GROUP STATE-->
                        
                        <!--FORM GROUP COUNTRY-->
                        <div class="form-group col-md-6">
                            <label for="country">Country</label>
                            <input type="text" 
                            placeholder = "Enter customers Country"
                            name="country"
                            id="country"
                            class = "form-control" 
                            value = "<?=$result[0]->country?>">
                            <?php
                            if($errors!="" && $errors->has('country'))
                            {
                                echo "<span class='error'>{$errors->first('country')}</span>";
                            }
                            ?>
                        </div>
                        <!--/FORM GROUP COUNTRY-->
                        
                        </div>
                        <div class="form-row">
                        <!--FORM GROUP TOWN-->
                        <div class="form-group col-md-6">
                            <label for="town">Town</label>
                            <input type="text" 
                            placeholder = "Enter customers Country"
                            name="town"
                            id="town"
                            class = "form-control" 
                            value = "<?=$result[0]->town?>">
                            <?php
                            if($errors!="" && $errors->has('town'))
                            {
                                echo "<span class='error'>{$errors->first('town')}</span>";
                            }
                            ?>
                        </div>
                        <!--/FORM GROUP TOWN-->
                        </div>
                    <button type = "submit" class="btn btn-primary" name="edit_customer" value="editCustomer"><i class="fa fa-check"></i> Submit</button>
                    </form>
                </div>              
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once(__DIR__. "/../includes/footer.php");?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!--DELETE MODAL-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Delete Customer</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?= BASEURL;?>helper/routing.php" method="POST">
          <div class="modal-body">
            <input type="hidden" name="csrf_token" id="csrf_token" value="<?= Session::getSession('csrf_token');?>">
            <input type="hidden" name="record_id" id="record_id">
            <p>Are you sure you want to delete the record?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" name="delete_customer">Delete</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--/DELETE MODAL-->


  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__."/../includes/core-scripts.php");?>
  <!--PAGE LEVEL SCRIPTS-->
  <?php require_once(__DIR__."/../includes/page-level/customer/manage-customer-scripts.php");?>
</body>
</html>
