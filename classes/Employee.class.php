<?php

require_once __DIR__."/../helper/requirements.php";

class Employee
{
    private $table = 'employees';
    private $database;
    protected $di;

    public function __construct(DependencyInjector $di)
    {

        $this->di = $di;
        $this->database = $this->di->get('database');        
    }

    private function validateData($data)
    {
        $validator = $this->di->get('validator');
        return $validator->check($data, [
            'name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255,
            ],
            'phone_no' => [
                'required' => true,
                // 'phone' => true
            ],
            'email_id' => [
                'required' => true,
                'email' => true
            ],
            'block_id' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'street' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'city' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'pincode' => [
                'required' => true,
                'minlength' => 6,
                'maxlength' => 6
            ],
            'state' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'country' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ]
        ]);
    }

    public function addEmployee($data)
    {
        $validation = $this->validateData($data);
        if(!$validation -> fails())
        {
            //validation was sucsessfully
            try
            {
                $this->database->beginTransaction();
                $this->table = "address";
                $data_to_be_inserted = ['block_id' => $data['block_id'],
                                        'street'  => $data['street'],
                                        'city'   => $data['city'],
                                        'pincode'   => $data['pincode'],
                                        'state'     => $data['state'],
                                        'country'     => $data['country']
                                    ];
                $address_id = $this->database->insert($this->table,$data_to_be_inserted);

                $this->table = "employees";
                $data_to_be_inserted = ['first_name' => $data['first_name'],
                                        'last_name' => $data['last_name'],
                                        'phone_no'   => $data['phone_no'],
                                        'email_id'   => $data['email_id'],
                                        'gender'     => $data['gender'],
                                        'address_id' => $address_id
                                    ];
                $employee_id = $this->database->insert($this->table,$data_to_be_inserted);

                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch (Exception $e) 
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            return VALIDATION_ERROR;
        }
    }

    public function getJSONDataForDataTable($draw,$searchParameter,$orderBy,$start,$length)
    {
        $columns = ['sr_no','first_name','last_name','phone_no','email_id','gender'];
        $totalRowCountQuery = "SELECT COUNT(id) as total_count FROM {$this->table} WHERE deleted = 0";
        $filteredRowCountQuery = "SELECT COUNT(id) as filtered_total_count FROM {$this->table} WHERE deleted = 0";

        $query = "SELECT `employees`.`id`,`employees`.`first_name`, `employees`.`last_name`, `employees`.`phone_no`, `employees`.`email_id`, `employees`.`gender` ,CONCAT(`address`.`block_no`,`address`.`street`,`address`.`city`,`address`.`pincode`,`address`.`state`,`address`.`country`,`address`.`town`) AS `address`FROM `employees` JOIN `address` ON `address`.`id` = `employees`.`id` WHERE `employees`.`deleted` = 0";
        if($searchParameter != null)
        {

            $query .= " AND `employees`.`first_name` like '%{$searchParameter}%' OR `employees`.`last_name` like '%{$searchParameter}%' OR `employees`.`phone_no` like '%{$searchParameter}%' OR `employees`.`email_id` like '%{$searchParameter}%' OR `address`.`block_no` like '%{$searchParameter}%' OR `address`.`street` like '%{$searchParameter}%' OR `address`.`city` like '%{$searchParameter}%' OR `address`.`pincode` like '%{$searchParameter}%' OR `address`.`state` like '%{$searchParameter}%' OR `address`.`country` like '%{$searchParameter}%' OR `address`.`town` like '%{$searchParameter}%'";

            $filteredRowCountQuery .= " AND `employees`.`first_name` like '%{$searchParameter}%' OR `employees`.`last_name` like '%{$searchParameter}%' OR `employees`.`phone_no` like '%{$searchParameter}%' OR `employees`.`email_id` like '%{$searchParameter}%' ";

        }
        if($orderBy != null)
        {
            $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
        }
        else
        {
            $query .= " ORDER BY {$columns[0]} ASC";
        }

        if($length != -1)
        {
            $query .= " LIMIT {$start}, {$length}";
            //This query is only for pagination
            // Here the query of $filteredRowCountQuery is not altered because suppose if the filtered data is 20 but if the length is 10 so that doesn't mean that only first 10 filtered record need to be displayed.

        }
        $this->table = "";

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        // Util::dd($totalRowCountResult);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;
        
        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->filtered_total_count : 0;

        $filteredData = $this->database->raw($query);
        $numberOfFilteredRowsToDisplay = is_array($filteredData) ? count($filteredData) : 0;
        // Util::dd($filteredData[0]->id);

        
        
        $data = [];
        for($i = 0; $i<$numberOfFilteredRowsToDisplay; $i++)
        {
            $subarray = [];
            $subarray[] = $i+1;
            $subarray[] = $filteredData[$i]->first_name;
            $subarray[] = $filteredData[$i]->last_name;
            $subarray[] = $filteredData[$i]->phone_no;
            $subarray[] = $filteredData[$i]->email_id;
            $subarray[] = $filteredData[$i]->gender;
            $subarray[] = $filteredData[$i]->address;
            $subarray[] = <<<BUTTONS
                <a href = "view-manage-employee.php?view_id={$filteredData[$i]->id}"class="view btn btn-outline-warning"><i class='fa fa-eye'></i></a>
                <a href = "edit-manage-employee.php?edit_id={$filteredData[$i]->id}" class="edit btn btn-outline-primary"><i class='fas fa-pencil-alt'></i></a>
                <button class='delete btn btn-outline-danger' id='{$filteredData[$i]->id}' data-toggle="modal" data-target="#deleteModal"><i class='fas fa-trash'></i></button>
BUTTONS;
            $data[] = $subarray;
            Session::setSession('old',$data);
        }
        $this->table = "employees";
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $numberOfTotalRows,
            "recordsFiltered" => $numberOfFilteredRows,
            "data" => $data
        );

        echo json_encode($output);
    }

    public function getEmployeeById($employeeId,$mode=PDO::FETCH_OBJ)
    {
        $query = "SELECT * FROM {$this->table} WHERE deleted = 0 AND id = {$employeeId}";
        $result = $this->database->raw($query,$mode);
        return $result;
    }

    public function update($data,$id)
    {
        // $validationData['name']=  $data['category_name'];
        $validation = $this->validateData($data);

        if(!$validation->fails())
        {
            try
            {
                $this->database->beginTransaction();
                $data_to_be_inserted = ['first_name' => $data['first_name'],
                                        'last_name'  => $data['last_name'],
                                        'phone_no'   => $data['phone_no'],
                                        'email_id'   => $data['email_id'],
                                        'gender'     => $data['gender']
                                    ];
                $this->database->update($this->table,$data_to_be_inserted,"id={$id}");

                $this->table = "address";
                $data_to_be_inserted = ['block_no'  => $data['block_no'],
                                        'street'    => $data['street'],
                                        'city'      => $data['city'],
                                        'pincode'   => $data['pincode'],
                                        'state'     => $data['state'],
                                        'country'   => $data['country'],
                                        'town'      => $data['town']
                                    ];
                $this->database->update($this->table,$data_to_be_inserted,"id={$id}");                                    
                $this->database->commit();
                return EDIT_SUCCESS;
            }
            catch(Exception $e)
            {
                $thhis->database->rollback();
                return EDIT_ERROR;
            }
        }
        else
        {
            return VALIDATION_ERROR;
        }
    }

    public function delete($id)
    {
        try
        {
            $this->database->beginTransaction();
            $this->database->delete($this->table,"id={$id}");
            $this->database->commit();
            return DELETE_SUCCESS;
        }
        catch(Exception $e)
        {
            $this->database->rollback();
            return DELETE_ERROR;
        }
    }
}