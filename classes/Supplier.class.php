<?php

require_once __DIR__."/../helper/requirements.php";

class Supplier
{
    private $table = 'suppliers';
    private $database;
    protected $di;

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');        
    }
    private function validateData($data)
    {
        $validator = $this->di->get('validator');
        return $validator->check($data, [
            'first_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255,
            ],
            'last_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'phone_no' => [
                'required' => true,
                // 'phone' => true
            ],
            'email_id' => [
                'required' => true,
                'email' => true
            ],
            'gst_no' => [
                'required' => true,
                // 'unique' => true
            ],
            'block_id' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'street' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'city' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'pincode' => [
                'required' => true,
                'minlength' => 6,
                'maxlength' => 6
            ],
            'state' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'country' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ]
        ]);
    }

    public function addSupplier($data)
    {
        $validation = $this->validateData($data);
        if(!$validation -> fails())
        {
            //validation was sucsessfully
            try
            {
                $this->database->beginTransaction();
                $data_to_be_inserted = ['first_name' => $data['first_name'],
                                        'last_name'  => $data['last_name'],
                                        'phone_no'   => $data['phone_no'],
                                        'email_id'   => $data['email_id'],
                                        'gst_no'     => $data['gst_no']
                                    ];
                $supplier_id = $this->database->insert($this->table,$data_to_be_inserted);

                $this->table = "address";
                $data_to_be_inserted = ['block_id' => $data['block_id'],
                                        'street'  => $data['street'],
                                        'city'   => $data['city'],
                                        'pincode'   => $data['pincode'],
                                        'state'     => $data['state'],
                                        'country'     => $data['country']
                                    ];
                $address_id = $this->database->insert($this->table,$data_to_be_inserted);

                $this->table = "address_supplier";
                $data_to_be_inserted = ['address_id' => $address_id,
                                        'supplier_id' => $supplier_id    
                                       ];
                $address_supplier_id = $this->database->insert($this->table,$data_to_be_inserted);
                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch (Exception $e) 
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            return VALIDATION_ERROR;
        }
    }
}

