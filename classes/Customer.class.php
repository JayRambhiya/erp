<?php

require_once __DIR__."/../helper/requirements.php";

class Customer
{
    private $table = 'customers';
    private $database;
    protected $di;

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');        
    }

    private function validateData($data)
    {
        $validator = $this->di->get('validator');
        return $validator->check($data, [
            'first_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255,
            ],
            'last_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'phone_no' => [
                'required' => true,
                // 'phone' => true
            ],
            'email_id' => [
                'required' => true,
                'email' => true
            ],
            'gst_no' => [
                'required' => true,
                // 'unique' => true
            ],
            'gender' => [
                'required' => true
            ],
            'block_no' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'street' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'city' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'pincode' => [
                'required' => true,
                'minlength' => 6,
                'maxlength' => 6
            ],
            'state' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'country' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ],
            'town' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
            ]
        ]);
    }

    public function addCustomer($data)
    {
        $validation = $this->validateData($data);
        if(!$validation -> fails())
        {
            //validation was sucsessfully
            try
            {
                $this->database->beginTransaction();
                $data_to_be_inserted = ['first_name' => $data['first_name'],
                                        'last_name'  => $data['last_name'],
                                        'phone_no'   => $data['phone_no'],
                                        'email_id'   => $data['email_id'],
                                        'gst_no'     => $data['gst_no'],
                                        'gender'     => $data['gender']
                                    ];
                $customer_id = $this->database->insert($this->table,$data_to_be_inserted);

                $this->table = "address";
                $data_to_be_inserted = ['block_no'  => $data['block_no'],
                                        'street'    => $data['street'],
                                        'city'      => $data['city'],
                                        'pincode'   => $data['pincode'],
                                        'state'     => $data['state'],
                                        'country'   => $data['country'],
                                        'town'      => $data['town']
                                    ];
                $address_id = $this->database->insert($this->table,$data_to_be_inserted);

                $this->table = "address_customer";
                $data_to_be_inserted = ['address_id' => $address_id,
                                        'customer_id' => $customer_id    
                                       ];
                $address_customer_id = $this->database->insert($this->table,$data_to_be_inserted);
                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch (Exception $e) 
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            return VALIDATION_ERROR;
        }
    }

    public function getJSONDataForDataTable($draw,$searchParameter,$orderBy,$start,$length)
    {
        //SELECT `customers`.`id`, `first_name`, `last_name`, `gst_no`, `phone_no`, `email_id`, `gender`,`address_id` FROM `customers` JOIN `address_customer` ON `address_customer`.`customer_id` = `customers`.`id`
        // Util::dd($this->table);
        // SELECT `customers`.`id`, `customers`.`first_name`, `customers`.`last_name`, `customers`.`gst_no`, `customers`.`phone_no`, `customers`.`email_id`, `customers`.`gender`,`address_customer`.`address_id` ,`address`.`block_no`,`address`.`street`, `address`.`city`,`address`.`pincode`,`address`.`state`,`address`.`country`,`address`.`town`FROM `customers` JOIN `address_customer` ON `address_customer`.`customer_id` = `customers`.`id` JOIN `address` ON `address`.`id` = `address_customer`.`address_id`
        $columns = ['sr_no','first_name','last_name','gst_no','phone_no','email_id','gender'];
        $totalRowCountQuery = "SELECT COUNT(id) as total_count FROM {$this->table} WHERE deleted = 0";
        $filteredRowCountQuery = "SELECT COUNT(id) as filtered_total_count FROM {$this->table} WHERE deleted = 0";
        $query = "SELECT `customers`.`id`, `customers`.`first_name`, `customers`.`last_name`, `customers`.`gst_no`, `customers`.`phone_no`, `customers`.`email_id`, `customers`.`gender`,`address_customer`.`address_id` ,CONCAT(`address`.`block_no`,`address`.`street`,`address`.`city`,`address`.`pincode`,`address`.`state`,`address`.`country`,`address`.`town`) AS `address`FROM `customers` JOIN `address_customer` ON `address_customer`.`customer_id` = `customers`.`id` JOIN `address` ON `address`.`id` = `address_customer`.`address_id` WHERE `customers`.`deleted` = 0";
        if($searchParameter != null)
        {

            $query .= " AND `customers`.`first_name` like '%{$searchParameter}%' OR `customers`.`last_name` like '%{$searchParameter}%' OR `customers`.`phone_no` like '%{$searchParameter}%' OR `customers`.`email_id` like '%{$searchParameter}%' OR `address`.`block_no` like '%{$searchParameter}%' OR `address`.`street` like '%{$searchParameter}%' OR `address`.`city` like '%{$searchParameter}%' OR `address`.`pincode` like '%{$searchParameter}%' OR `address`.`state` like '%{$searchParameter}%' OR `address`.`country` like '%{$searchParameter}%' OR `address`.`town` like '%{$searchParameter}%'";

            $filteredRowCountQuery .= " AND `customers`.`first_name` like '%{$searchParameter}%' OR `customers`.`last_name` like '%{$searchParameter}%' OR `customers`.`phone_no` like '%{$searchParameter}%' OR `customers`.`email_id` like '%{$searchParameter}%' ";

        }
        if($orderBy != null)
        {
            $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
        }
        else
        {
            $query .= " ORDER BY {$columns[0]} ASC";
        }

        if($length != -1)
        {
            $query .= " LIMIT {$start}, {$length}";
            //This query is only for pagination
            // Here the query of $filteredRowCountQuery is not altered because suppose if the filtered data is 20 but if the length is 10 so that doesn't mean that only first 10 filtered record need to be displayed.

        }
        $this->table = "";

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        // Util::dd($totalRowCountResult);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;
        
        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->filtered_total_count : 0;

        $filteredData = $this->database->raw($query);
        $numberOfFilteredRowsToDisplay = is_array($filteredData) ? count($filteredData) : 0;
        // Util::dd($filteredData[0]->id);

        
        
        $data = [];
        for($i = 0; $i<$numberOfFilteredRowsToDisplay; $i++)
        {
            // $customer_id = $filteredData[$i]->id;
            // // echo $customer_id;
            // $this->table = "address_customer";
            // $addressCustomerQuery = "SELECT address_id FROM {$this->table} WHERE customer_id = $customer_id";
            // $addressCustomerQueryResult = $this->database->raw($addressCustomerQuery);
            // // Util::dd($addressQueryResult[0]->address_id);   
            // $addressId =  $addressCustomerQueryResult[0]->address_id;
            // $this->table = "address";
            // $addressQuery = "SELECT * FROM {$this->table} WHERE id = $addressId";
            // $addressQueryResult = $this->database->raw($addressQuery);
            // Util::dd($addressQueryResult[0]);
            // $address = $filteredData[$i]->block_no .' ,'.$filteredData[$i]->street .' ,'.$filteredData[$i]->city .' ,'.$filteredData[$i]->pincode .' ,'.$filteredData[$i]->state .' ,'.$filteredData[$i]->country .' ,'.$filteredData[$i]->town;
            // Util::dd($address);
            $subarray = [];
            $subarray[] = $i+1;
            $subarray[] = $filteredData[$i]->first_name;
            $subarray[] = $filteredData[$i]->last_name;
            $subarray[] = $filteredData[$i]->gst_no;
            $subarray[] = $filteredData[$i]->phone_no;
            $subarray[] = $filteredData[$i]->email_id;
            $subarray[] = $filteredData[$i]->gender;
            $subarray[] = $filteredData[$i]->address;
            $subarray[] = <<<BUTTONS
                <a href = "view-manage-customer.php?view_id={$filteredData[$i]->id}"class="view btn btn-outline-warning"><i class='fa fa-eye'></i></a>
                <a href = "edit-manage-customer.php?edit_id={$filteredData[$i]->id}" class="edit btn btn-outline-primary"><i class='fas fa-pencil-alt'></i></a>
                <button class='delete btn btn-outline-danger' id='{$filteredData[$i]->id}' data-toggle="modal" data-target="#deleteModal"><i class='fas fa-trash'></i></button>
BUTTONS;
            $data[] = $subarray;
            Session::setSession('old',$data);
        }
        $this->table = "customers";
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $numberOfTotalRows,
            "recordsFiltered" => $numberOfFilteredRows,
            "data" => $data
        );

        echo json_encode($output);
    }

    public function getCustomerById($customerId,$mode=PDO::FETCH_OBJ)
    {
        $query = "SELECT * FROM {$this->table} WHERE deleted = 0 AND id = {$customerId}";
        $result = $this->database->raw($query,$mode);
        return $result;
    }

    public function update($data,$id)
    {
        // $validationData['name']=  $data['category_name'];
        $validation = $this->validateData($data);

        if(!$validation->fails())
        {
            try
            {
                $this->database->beginTransaction();
                $data_to_be_inserted = ['first_name' => $data['first_name'],
                                        'last_name'  => $data['last_name'],
                                        'phone_no'   => $data['phone_no'],
                                        'email_id'   => $data['email_id'],
                                        'gst_no'     => $data['gst_no'],
                                        'gender'     => $data['gender']
                                    ];
                $this->database->update($this->table,$data_to_be_inserted,"id={$id}");
                // $customer_id = $this->database->insert($this->table,$data_to_be_inserted);

                $this->table = "address";
                $data_to_be_inserted = ['block_no'  => $data['block_no'],
                                        'street'    => $data['street'],
                                        'city'      => $data['city'],
                                        'pincode'   => $data['pincode'],
                                        'state'     => $data['state'],
                                        'country'   => $data['country'],
                                        'town'      => $data['town']
                                    ];
                $this->database->update($this->table,$data_to_be_inserted,"id={$id}");                                    
                $this->database->commit();
                return EDIT_SUCCESS;
            }
            catch(Exception $e)
            {
                $thhis->database->rollback();
                return EDIT_ERROR;
            }
        }
        else
        {
            return VALIDATION_ERROR;
        }
    }

    public function delete($id)
    {
        try
        {
            $this->database->beginTransaction();
            $this->database->delete($this->table,"id={$id}");
            $this->database->commit();
            return DELETE_SUCCESS;
        }
        catch(Exception $e)
        {
            $this->database->rollback();
            return DELETE_ERROR;
        }
    }

    public function getCustomerByEmailId($customer_email){
        // SELECT id FROM `customers` WHERE email_id = 'mayurnetake20@gmail.com' and deleted = 0
        $query = "SELECT id FROM `customers` WHERE email_id = '{$customer_email}' and deleted = 0";
        $result = $this->database->raw($query,$mode = PDO::FETCH_ASSOC);
        return $result;
    }
}