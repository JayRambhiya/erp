<?php
require_once __DIR__."/../helper/requirements.php";

class Sales{
    private $table = "sales";
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }

    public function createSales($customer_id,$products,$quantity,$discount){
        try
        {
            $this->database->beginTransaction();
            // Util::dd($customer_id);
            $data_to_be_inserted = [
                'customer_id' => $customer_id
            ];
            $invoice_id = $this->database->insert('invoice',$data_to_be_inserted);
            // Util::dd($invoice_id);
            for($i = 0;$i<count($products);$i++){
                $data_to_be_inserted = [
                    'product_id' => $products[$i],
                    'quantity' => $quantity[$i],
                    'discount' => $discount[$i],
                    'invoice_id' => $invoice_id
                ];
                // echo ("Product id:" + $products[$i]);
                $sales_id = $this->database->insert('sales',$data_to_be_inserted);
                echo ($sales_id);
                echo '<br>';
                $this->database->commit();
                return $invoice_id;
            }
            // Util::dd('end');
        }
        catch(Exception $e)
        {
            $this->database->rollback();
            return -1;
        }
    }
}