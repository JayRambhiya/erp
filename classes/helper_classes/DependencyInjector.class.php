<?php
//This class is made for avoinding the dependency injection that we were doing in the prevoius project.
//Because of this project we will just need the object of this class and we will get all the objects of other classes.
class DependencyInjector
{
    private $dependencies = [];
    //This method is for setting the dependencies in the private array
    public function set(string $key,$value)
    {
        $this->dependencies[$key] = $value;
    }
    //This method is for getting the object of the dependency that you passed in the argument
    public function get(string $key)
    {
        if(isset($this->dependencies[$key])){
            return $this->dependencies[$key];
        }
        die('This dependency Not Found: ' . $key);
    }
}