<?php
//This class is made to get all the configs by a method so by this class we can get all the configs from whichever file we want just by a object of this class.
class Config
{
    protected $config;
    public function __construct()
    {
        $this->config = parse_ini_file(__DIR__ . "/../../config.ini");
    }

    public function get(string $key)
    {
        if(isset($this->config[$key]))
        {
            return $this->config[$key];
        }
        die("This Config cannot be found : " . $key);
    }
}