<?php
require_once __DIR__ . './helper/init.php';
?>
<!DOCTYPE html>
<html lang="en">

<?php require_once __DIR__ . './views/includes/head-section.php';?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php require_once __DIR__ . "./views/includes/sidebar.php";?>


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

       <?php require_once __DIR__ . "/../includes/navbar.php";?>

        

      </div>
      <!-- End of Main Content -->

      <?php require_once __DIR__ . '/../includes/footer.php' ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <?php require_once __DIR__ . "/../includes/scroll-to-top.php";?>

  <?php require_once __DIR__ . '/../includes/core-scripts.php' ?>

  <!-- Page level plugins -->
  <script src="<?= BASEASSETS; ?>vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?= BASEASSETS; ?>js/demo/chart-area-demo.js"></script>
  <script src="<?= BASEASSETS; ?>js/demo/chart-pie-demo.js"></script>

</body>

</html>
