console.log('ter');
var id = 2;
var baseURL = "http://localhost/Back-end/OOP/OOP-PHP-ERP";
var filePath = "/helper/routing.php";
function deleteProduct(delete_id){
    var elements = document.getElementsByClassName("product_row");
    // var element_id = $(this).attr('id').split("_")[1];
    console.log("DELETE ID"+delete_id);
    if(elements.length != 1){
        // $deleted_finalrate = $('#finalrate_'+delete_id).val();
        // $final_total = $('#finalTotal').val();
        // $('#finalTotal').val(+$final_total - +$deleted_finalrate);
        $("#element_"+delete_id).remove();
        $.fn.updateTotal(0,delete_id);
    }
    // $.fn.updateTotal(0,delete_id);
}
function addProduct(){
    $("#products_container").append(`
    <div class="row product_row" id = "element_1">
                                <!-- BEGIN: CATEGORY SELECT -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="">Category </label>
                                        <select id="category_${id}" class = "form-control category_select">
                                            <option disabled selected>Select Category</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- END: CATEGORY SELECT -->

                                <!-- BEGIN: PRODUCT SELECT -->
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="">Products </label>
                                        <select name = "product_id[]" id="product_${id}" class = "form-control product_select">
                                            <option disabled selected>Select Product</option>  
                                        </select>
                                    </div>
                                </div>
                                <!-- END: PRODUCT SELECT -->

                                <!-- BEGIN: SELLING PRICE -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="">Selling Price</label>
                                        <input type="number" id = "selling_price_${id}" class = "form-control selling_price_input" disabled value = "0">
                                    </div>
                                </div>
                                <!-- END: SELLING PRICE -->

                                <!-- BEGIN: QUANTITY -->
                                <div class="col-md-1">
                                    <div class="form-group">
                                    <label for="">Quantity</label>
                                        <input type="number" name = "quantity[]" id = "quantity_${id}" class = "form-control quantity_input" min = "0"value = 0>
                                    </div>
                                </div>
                                <!-- END: QUANTITY -->

                                <!-- BEGIN: DISCOUNT -->
                                <div class="col-md-1">
                                    <div class="form-group">
                                    <label for="">Discount(%)</label>
                                        <input type="number" name = "discount[]" id = "discount_${id}" class = "form-control discount_input" min = "0"value = 0>
                                    </div>
                                </div>
                                <!-- END: DISCOUNT -->

                                <!-- BEGIN: FINAL RATE -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="">Final Rate</label>
                                        <input type="number" id = "finalrate_${id}" class = "form-control finalrate_input" value = 0 readonly>
                                    </div>
                                </div>
                                <!-- END: DISCOUNT -->

                                <!--BEGIN: DELETE BUTTON -->
                                <div class="col-md-1">
                                    <button onclick = "deleteProduct(${id})" type = "button" class = "btn btn-danger" style = "margin-top: 40%">
                                    <i class = "far fa-trash-alt"></i>
                                    </button>
                                </div>
                                <!--END: DELETE BUTTON -->
                            </div>`
    );
    $.ajax({
      url:baseURL+filePath,
      method:'POST',
      data:{
        getCategories:true
      },
      dataType: 'json',
      success:function(categories){
        categories.forEach(function (category){
            // console.log(id);
          $("#category_"+id).append(
            `<option value = '${category.id}'>${category.name}</option>`
          );
        });
        id++;
      }
    });
}
/**
 * Sysntax of the below method is:
 * $('selector').on('event','target','function(){}')
 */
$("#products_container").on('change','.category_select',function(){
  var element_id = $(this).attr('id').split("_")[1];
  var category_id = this.value;
  $.ajax(
    {
      url:baseURL+filePath,
      method:'POST',
      data:{
        getProductsByCategoryID: true,
        categoryID:category_id
      },
      dataType:'json',
      success:function(products){
        $("#product_"+element_id).empty();
        $('#selling_price_'+element_id).val('0');
        $('#discount_'+element_id).val('0');
        $('#finalrate_'+element_id).val('0');
        $('#quantity_'+element_id).val('0');
        //Empty marna padega bcoz apan append karrahe hai so agar dusra category select karenge toh products pervious category ke aur selected category ke append hojaenge
        $("#product_"+element_id).append("<option disabled selected>Select Product</option>");
        products.forEach(function (product){
          $("#product_"+element_id).append(
            `<option value = '${product.id}'>${product.name}</option>`
          );
        });
      }
    })
});

$("#products_container").on('change','.product_select',function(){
    var element_id = $(this).attr('id').split('_')[1];
    var product_id = this.value;
    // console.log(element_id);
    // console.log(product_id);
    $.ajax({
        url:baseURL+filePath,
        method:'POST',
        data:{
            getSellingPriceByProductID:true,
            productID:product_id
        },
        dataType:'json',
        success:function(selling_price){
            // console.log('#selling_price_'+element_id);
            $('#selling_price_'+element_id).val(selling_price[0].selling_rate);
            $('#quantity_'+element_id).val('1');
            $('#discount_'+element_id).val('0');
            $('#finalrate_'+element_id).val(selling_price[0].selling_rate);
            $.fn.updateTotal(selling_price[0].selling_rate,element_id);
        }
    })
});

$("#products_container").on('change','.quantity_input',function(){
    var element_id = $(this).attr('id').split('_')[1];
    // console.log('In quantity');
    // console.log($('#quantity_'+element_id).val());
    $quantity = $('#quantity_'+element_id).val();
    $discount = $('#discount_'+element_id).val();
    $selling_price = $('#selling_price_'+element_id).val();
    $final_rate = $selling_price*$quantity;
    // $('#finalrate_'+element_id).val($selling_price*$quantity);
    if($discount != 0 && 1){
        $final_discounted_rate = $final_rate-(($final_rate*$discount)/100);
        $('#finalrate_'+element_id).val($final_discounted_rate);
        $.fn.updateTotal($final_discounted_rate,element_id);
    }else{
        $('#finalrate_'+element_id).val($final_rate);  
        $.fn.updateTotal($final_rate,element_id);
    }
    
});

$("#products_container").on('change','.discount_input',function(){
    var element_id = $(this).attr('id').split('_')[1];
    $final_rate = $('#finalrate_'+element_id).val();
    $discount = $('#discount_'+element_id).val();
    $quantity = $('#quantity_'+element_id).val();
    $selling_price = $('#selling_price_'+element_id).val();
    // console.log("Final Rate"+$final_rate);
    // console.log("Discount per"+$discount);
    // $discount_rate = ($final_rate*1)/$discount;
    // console.log("discount:"+$discount_rate);
    if($discount != 0){
        console.log("enter if");
        $final_discounted_rate = $final_rate-(($final_rate*$discount)/100);
        $('#finalrate_'+element_id).val($final_discounted_rate);
        $.fn.updateTotal($final_discounted_rate,element_id);
    }else{
        console.log("enter else");
        $final_discounted_rate = $selling_price*$quantity;
        $('#finalrate_'+element_id).val($final_discounted_rate);
        $.fn.updateTotal($final_discounted_rate,element_id);
    }
    // $final_discounted_rate = $final_rate-(($final_rate*1)/$discount);
    // console.log("final discounted rate:"+$final_discounted_rate);
    // $('#finalrate_'+element_id).val($final_discounted_rate);
});
$final_rates = [];
$.fn.updateTotal = function($final_rate,element_id){
    $final_rates[0] = 0;
    $final_total = 0;
    console.log(element_id);
    $final_rates[element_id] = $final_rate;
    console.log('after assg'+$final_rates.length);
    for($i = 0; $i < $final_rates.length; $i++){
        console.log("RATES");
        console.log($final_rates[$i]);
        $final_total = +$final_total + +$final_rates[$i];
    }
    $('#finalTotal').val($final_total);
    // $pervious_final_rate = $('#finalTotal').val();
    // if(element_id > 1){
    //     console.log('enter total if');
    //     $final_total = +$pervious_final_rate + +$final_rate;
    //     $('#finalTotal').val($final_total);
    // }else{
    //     console.log('enter total else');
    //     $('#finalTotal').val(+$final_rate + +$pervious_final_rate);
    // }
}
//final_rate - (final_rate*1/discount_rate)

function verifyEmail(){
    $customer_email = $('#customer_email').val();
    // console.log($customer_email);
    $.ajax({
        url:baseURL+filePath,
        method:'POST',
        data:{
            getCustomerIdByEmail:true,
            customer_email:$customer_email
        },
        dataType: 'json',
        success:function($customer_id){
            if($customer_id.length != 0){
                $('#email_verify_fail').removeClass('d-inline-block');
                $('#email_verify_fail').addClass('d-none');
                $('#add_customer_btn').removeClass('d-inline-block');
                $('#add_customer_btn').addClass('d-none');
                $('#customer_id').val($customer_id[0].id);
                $('#email_verify_success').removeClass('d-none');
                $('#email_verify_success').addClass('d-inline-block');
                $('#add_sales').attr('disabled',false);
            }else{
                $('#customer_id').val("");
                $('#email_verify_success').removeClass('d-inline-block');
                $('#email_verify_success').addClass('d-none');
                $('#email_verify_fail').removeClass('d-none');
                $('#email_verify_fail').addClass('d-inline-block');
                $('#add_customer_btn').removeClass('d-none');
                $('#add_customer_btn').addClass('d-inline-block');
            }
        }

    });    
}