var TableDataTables = function(){
    var handleSupplierTable = function(){
        var manageEmployeeTable = $("#manage-supplier-datatable");
        var baseURL = "http://localhost/Back-end/OOP/OOP-PHP-ERP";
        var filePath = "/helper/routing.php";
        // console.log(baseURL);
        var oTable = manageSupplierTable.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "page": "manage_supplier"
                }
            },
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            "order": [
                [1, "ASC"]
            ],
            "columnDefs": [{
                "targets": [0,-1],
                'orderable': false
            }],
        });


    return {
        init: function(){
            handleEmployeeTable();
        }
    }
}();
//It’s an Immediately-Invoked Function Expression, or IIFE 
jQuery(document).ready(function(){
    TableDataTables.init();
});