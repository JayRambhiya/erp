// alert('hi');
var TableDataTables = function(){
    var handleCustomerTable = function(){
        var manageCustomerTable = $("#manage-customer-datatable");
        var baseURL = "http://localhost/Back-end/OOP/OOP-PHP-ERP";
        var filePath = "/helper/routing.php";
        // console.log(baseURL);
        var oTable = manageCustomerTable.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "page": "manage_customer"
                }
            },
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            "order": [
                [1, "ASC"]
            ],
            "columnDefs": [{
                "targets": [0,-1],
                'orderable': false
            }],
        });

        //Another way for event deligation
        // var manageViewPage = $('#view-page');
        // manageViewPage.on('load', function(){
        //     alert('in');
        //     id = $_GET['view_id'];
        //     console.log(id);
        //     $("#customer_id").val(id);
        //     $.ajax({
        //         url: baseURL + filePath,
        //         method: "POST",
        //         data: {
        //             "customer_id": id,
        //             "fetch": "customer"
        //         },
        //         dataType: "json",
        //         success: function(data){
        //             $("#category_name").val(data[0].name);
        //         }
        //     });
        // });

        manageCustomerTable.on('click', '.edit', function(){
            id = $(this).attr('id');
            $("#category_id").val(id);
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "category_id": id,
                    "fetch": "category"
                },
                dataType: "json",
                success: function(data){
                    $("#category_name").val(data[0].name);
                }
            });
        });
        manageCustomerTable.on('click', '.delete', function(){
            console.log("Entered");
            id = $(this).attr('id');
            $("#record_id").val(id);
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "customer_id": id,
                    "fetch": "customer"
                },
            });
        });
    }
    return {
        init: function(){
            handleCustomerTable();
        }
    }
}();
//It’s an Immediately-Invoked Function Expression, or IIFE 
jQuery(document).ready(function(){
    TableDataTables.init();
});