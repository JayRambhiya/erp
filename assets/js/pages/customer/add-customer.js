$(function(){
    $("#add-customer").validate({
        'rules': {
            'first_name':{
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'last_name':{
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'phone_no':{
                required: true,
            },
            'email_id':{
                required: true,
            },
            'gst_no':{
                required: true,
            },
            'gender':{
                required: true,
            },
            'block_id':{
                required: true,
            },
            'street':{
                required: true,
            },
            'city':{
                required: true,
            },
            'pincode':{
                required: true,
            },
            'state':{
                required: true,
            },
            'country':{
                required: true,
            }

        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });
});