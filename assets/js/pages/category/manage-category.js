var TableDataTables = function(){
    var handleCategoryTable = function(){
        var manageCategoryTable = $("#manage-category-datatable");
        var baseURL = "http://localhost/Back-end/OOP/OOP-PHP-ERP";
        var filePath = "/helper/routing.php";
        console.log(baseURL);
        
        var oTable = manageCategoryTable.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "page": "manage_category"
                }
            },
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            "order": [
                [1, "ASC"]
            ],
            "columnDefs": [{
                'orderable': false,
                "targets": [0, -1]
            }],
        });

        //Another way for event deligation
        manageCategoryTable.on('click', '.edit', function(){
            id = $(this).attr('id');
            $("#category_id").val(id);
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "category_id": id,
                    "fetch": "category"
                },
                dataType: "json",
                success: function(data){
                    $("#category_name").val(data[0].name);
                }
            });
        });
        manageCategoryTable.on('click', '.delete', function(){
            console.log("Entered");
            id = $(this).attr('id');
            $("#record_id").val(id);
            $.ajax({
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "category_id": id,
                    "fetch": "category"
                },
            });
        });
    }
    return {
        init: function(){
            handleCategoryTable();
        }
    }
}();
//It’s an Immediately-Invoked Function Expression, or IIFE 
jQuery(document).ready(function(){
    TableDataTables.init();
});