$(function(){
    $("#add-customer").validate({
        'rules': {
            'name':{
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'phone_no':{
                required: true,
            },
            'email_id':{
                required: true,
            },
            'address_id':{
                required:true,
            },
            'access':{
                required: true,
            },
            'block_id':{
                required: true,
            },
            'street':{
                required: true,
            },
            'city':{
                required: true,
            },
            'pincode':{
                required: true,
            },
            'state':{
                required: true,
            },
            'country':{
                required: true,
            }

        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });
});