<?php

//routing file is created for clean coding and categorizing. Error handling and directing after the action is performed in the same file without exposing the name of the file from the action is generated.
require_once 'init.php';

if(isset($_POST['add_category']))
{
    echo 'entered';
//     echo $_POST['csrf_token'];
    //USER HAS REQUESTED TO ADD A NEW CATEGORY
    // Util::dd(Session::getSession('csrf_token'));
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        // Util::dd('entered cate');
        echo 'validated';
        $result = $di->get('category')->addCategory($_POST);
        // dd($result);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-category.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-category.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                //Serializing an object means converting it to a bytestream representation that can be stored in a file. This is useful for persistent data; for example, PHP sessions automatically save and restore objects.(in my own words: serialize is a concept in programming lanuage with the help of which an object can be stored in a file with the datatype of object unlike anyother text which is stored in string format i.e. it will be stored in the secondary memory when not used. Object that is serialized by the serialize() method can be used again by unserializing it.)

                Session::setSession('old', $_POST);
                Util::redirect('add-category.php');
                break;
        }
    }
}
if(isset($_POST['add_customer'])) {
    //USER HAS REQUESTED TO ADD A NEW CUSTOMER
    // Util::dd('entered');
    if(isset($_POST['csrf_token']) && util::verifyCSRFToken($_POST))
    {

        $result = $di->get('customer')->addCustomer($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-customer.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-customer.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-customer.php');
                break;
        }
    }
}
if(isset($_POST['add_supplier'])) {
    //USER HAS REQUESTED TO ADD A NEW CUSTOMER
    // Util::dd('entered');
    if(isset($_POST['csrf_token']) && util::verifyCSRFToken($_POST))
    {

        $result = $di->get('supplier')->addSupplier($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-supplier.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-supplier.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-supplier.php');
                break;
        }
    }
}

if(isset($_POST['add_employee'])) {
    //USER HAS REQUESTED TO ADD A NEW CUSTOMER
    // Util::dd('entered');
    if(isset($_POST['csrf_token']) && util::verifyCSRFToken($_POST))
    {

        $result = $di->get('employee')->addEmployee($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-employee.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-employee.php');
                break;
        }
    }
}

if(isset($_POST['page']) && $_POST['page'] == 'manage_category')
{
    // Util::dd("Reached");
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    //Util::dd(getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length));
    $di->get('category')->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
}

if(isset($_POST['fetch']) && $_POST['fetch'] == 'category')
{
    $category_id = $_POST['category_id'];
    $result = $di->get('category')->getCategoryById($category_id, PDO::FETCH_ASSOC);
//    Util::dd($result);
    echo json_encode($result);
}

if(isset($_POST['edit_category']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('category')->update($_POST,$_POST['category_id']);
        switch($result)
        {
            case EDIT_ERROR:
                Session::setSession(EDIT_ERROR,"There was a problem while editing record,please try again later!");
                Util::redirect('manage-category.php');
            break;
            case EDIT_SUCCESS:
                Session::setSession(EDIT_SUCCESS,'The record have been edited successfully!');
                Util::redirect('manage-category.php');
            break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR,'There was some problem in validating your date at server side!');
                Session::setSession('errors',serialize($di->get('validation')->errors()));
                Session::setSession('old',$_POST);
                Util::redirect('manage-category.php');
            break;
        }
    }
}

if(isset($_POST['delete_category']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('category')->delete($_POST['record_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR,'There was a problem while deleting record, please try again later!');
                Util::redirect('manage-category.php');
            break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS,'The record have been successfully deleted!');
                Util::redirect('manage-category.php');
            break;
        }
    }
}

if(isset($_POST['page']) && $_POST['page'] == 'manage_customer')
{
    // Util::dd("Reached");
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    //Util::dd(getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length));
    $di->get('customer')->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
}
if(isset($_POST['edit_customer'])) {
    //USER HAS REQUESTED TO ADD A NEW CUSTOMER
    // Util::dd('entered');
    if(isset($_POST['csrf_token']) && util::verifyCSRFToken($_POST))
    {
        // Util::dd($_POST);
        $result = $di->get('customer')->update($_POST,$_POST['customer_id']);
        // echo "In edit";
        // Util::dd($result);
        switch($result)
        {
            case EDIT_ERROR:
                Session::setSession(EDIT_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-customer.php');
                break;
            case EDIT_SUCCESS:
                // Util::dd("In success");
                Session::setSession(EDIT_SUCCESS, 'The record have been updated successfully!');
                Session::setSession(RELOAD_TABLE,'');
                // $di->get('customer')->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
                // Util::dd();
                Util::redirect('manage-customer.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-customer.php');
                break;
        }
    }
}
if(isset($_POST['delete_customer']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('customer')->delete($_POST['record_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR,'There was a problem while deleting record, please try again later!');
                Util::redirect('manage-customer.php');
            break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS,'The record have been successfully deleted!');
                Util::redirect('manage-customer.php');
            break;
        }
    }
}
if(isset($_POST['fetch']) && $_POST['fetch'] == 'customer')
{
    $customer_id = $_POST['customer_id'];
    $result = $di->get('customer')->getCategoryById($category_id, PDO::FETCH_ASSOC);
    Util::dd($result);
    echo json_encode($result);
}


if(isset($_POST['page']) && $_POST['page'] == 'manage_employee')
{
    // Util::dd("Reached");
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    //Util::dd(getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length));
    $di->get('employee')->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
}

if(isset($_POST['edit_employee'])) {
    //USER HAS REQUESTED TO ADD A NEW CUSTOMER
    // Util::dd('entered');
    if(isset($_POST['csrf_token']) && util::verifyCSRFToken($_POST))
    {
        // Util::dd($_POST);
        $result = $di->get('employee')->update($_POST,$_POST['employee_id']);
        // echo "In edit";
        // Util::dd($result);
        switch($result)
        {
            case EDIT_ERROR:
                Session::setSession(EDIT_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case EDIT_SUCCESS:
                // Util::dd("In success");
                Session::setSession(EDIT_SUCCESS, 'The record have been updated successfully!');
                Session::setSession(RELOAD_TABLE,'');
                // $di->get('customer')->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
                // Util::dd();
                Util::redirect('manage-employee.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('edit-manage-employee.php');
                break;
        }
    }
}
if(isset($_POST['delete_employee']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('employee')->delete($_POST['record_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR,'There was a problem while deleting record, please try again later!');
                Util::redirect('manage-employee.php');
            break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS,'The record have been successfully deleted!');
                Util::redirect('manage-employee.php');
            break;
        }
    }
}
if(isset($_POST['fetch']) && $_POST['fetch'] == 'employee')
{
    $employee_id = $_POST['employee_id'];
    $result = $di->get('customer')->getCategoryById($category_id, PDO::FETCH_ASSOC);
    Util::dd($result);
    echo json_encode($result);
}

/****************************************************************
 ****************************PRODUCT MANAGEMENT******************
 ****************************************************************/

if(isset($_POST['add_product']))
{
    //USER HAS REQUESTED TO ADD A NEW CATEGORY
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('product')->addProduct($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-product.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-product.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR, 'There was some problem in validating your data at server side!');
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-product.php');
                break;
        }
    }
}

if(isset($_POST['page']) && $_POST['page'] == 'manage_product')
{
    // Util::dd("Reached");
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    //Util::dd(getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length));
    $di->get('product')->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
}

if(isset($_POST['delete_product']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('product')->delete($_POST['record_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR,'There was a problem while deleting record, please try again later!');
                Util::redirect('manage-product.php');
            break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS,'The record have been successfully deleted!');
                Util::redirect('manage-product.php');
            break;
        }
    }
}

if(isset($_POST['getCategories'])){
    echo json_encode($di->get('category')->all());
}

if(isset($_POST['getProductsByCategoryID'])){
    $category_id = $_POST['categoryID'];
    echo json_encode($di->get('product')->getProductsByCategoryID($category_id));
}

if(isset($_POST['getSellingPriceByProductID'])){
    $product_id = $_POST['productID'];
    echo json_encode($di->get('product')->getSellingPriceByProductID($product_id));
}
if(isset($_POST['getCustomerIdByEmail'])){
    $customer_email = $_POST['customer_email'];
    // Util::dd($customer_email);
    $result = json_encode($di->get('customer')->getCustomerByEmailId($customer_email));
    // Util::dd($result);
    echo $result;
}

if(isset($_POST['add_sales'])){
    // Util::dd($_POST);
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $customer_id = $_POST['customer_id'];
        $product_id = $_POST['product_id'];
        $quantity = $_POST['quantity'];
        $discount = $_POST['discount'];
        $result = $di->get('sales')->createSales($customer_id,$product_id,$quantity,$discount);
        if($result != -1){
            Util::redirect("invoice.php?id={$result}");
        }else{
            Util::redirect("sales.php");
        }
    }
}
// CSRF - Cross-site Request Frogery